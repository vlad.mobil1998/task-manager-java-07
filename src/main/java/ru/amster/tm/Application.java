package ru.amster.tm;

import ru.amster.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String readTerminalCommand = scanner.nextLine();
            parseArgs(readTerminalCommand);
        }
    }

    private static void parseArgs(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.HELP:
                showHelpMsg();
                break;
            case TerminalConst.ABOUT:
                showAboutMsg();
                break;
            case TerminalConst.VERSION:
                showVersionMsg();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                System.out.println("ERROR: Command not late");
                break;
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArgs(arg);
        return true;
    }

    private static void showHelpMsg() {
        System.out.println(TerminalConst.ABOUT + " - Show developer info.");
        System.out.println(TerminalConst.VERSION + " - Show version info.");
        System.out.println(TerminalConst.HELP + " - Display terminal commands.");
        System.out.println(TerminalConst.EXIT + " - Close application.");
    }

    private static void showAboutMsg() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Amster Vladislav");
        System.out.println("E-MAIL: vlad@amster.ru");
    }

    private static void showVersionMsg() {
        System.out.println("[VERSION]");
        System.out.println("1.0.6");
    }

}
